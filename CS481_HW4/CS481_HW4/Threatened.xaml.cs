﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;



namespace CS481_HW4
{
    // [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Threatened : ContentPage
    {

        ObservableCollection<Animal> listOfAnimals;
        Animal[] animals;
        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }

        //populates the animal list
        public void Populate()
        {
            listOfAnimals.Add(new Animal("Lion", "A lion’s life is all about sleeping, napping, and resting.", "lions.jpg"));
            listOfAnimals.Add(new Animal("Southern Cassowary", "One of the largest birds on the planet. ",
                "cassowary.jpg"));
            listOfAnimals.Add(new Animal("Secretary Bird", "Standing over four feet tall in the tall grass of Africa",
                "secretary_bird.jpg"));
            listOfAnimals.Add(new Animal("Elephant", "You can travel to Africa, of course, but the easiest way to observe African elephants is at the Safari Park!",
                "elephants.jpg"));
            listOfAnimals.Add(new Animal("Platypus", "A duck-like bill, a fur coat, big webbed feet, the platypus looks like no other mammal.",
                "platypus.jpg"));
            AnimalsList.ItemsSource = listOfAnimals;
        }

        public Threatened()
        {
            InitializeComponent();
            listOfAnimals = new ObservableCollection<Animal>();
            Populate();
        }


        //Handle the refreshing of each page
        void Handle_Refreshing(Object sender, System.EventArgs e)
        {
            listOfAnimals.Clear();
            Populate();
            AnimalsList.IsRefreshing = false;
        }

        //Remove an item from the animal list
        void Remove_Clicked(object sender, EventArgs e)
        {
            var ani = (MenuItem)sender;
            Animal item = (Animal)ani.CommandParameter;
            listOfAnimals.Remove(item);
        }

        //open page connected to item in the list
        async void ViewCell_Tapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var ani = (ListView)sender;
            Animal item = (Animal)ani.SelectedItem;
            if (item.Title == "Lion")
            {
                Navigation.PushAsync(new Lions());
            }
            if (item.Title == "Southern Cassowary")
            {
                Navigation.PushAsync(new Cassowary());
            }
            if (item.Title == "Secretary Bird")
            {
                Navigation.PushAsync(new SecretaryBird());
            }
            if (item.Title == "Elephant")
            {
                Navigation.PushAsync(new Elephant());
            }
            if (item.Title == "Platypus")
            {
                Navigation.PushAsync(new Platypus());
            }
        }
    }
}