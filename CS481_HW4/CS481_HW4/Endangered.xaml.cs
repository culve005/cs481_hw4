﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;



namespace CS481_HW4
{
    // [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Endangered : ContentPage
    {

        ObservableCollection<Animal> listOfAnimals;
        Animal[] animals;
        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }


        //populates the animal list
        public void Populate()
        {
            listOfAnimals.Add(new Animal("Matschie’s Tree Kangaroo", "Safari Park guests can see tree kangaroos in their habitat in Walkabout Australia.", "matschies.jpg"));
            listOfAnimals.Add(new Animal("California Condor", "Most of our condors live in their off-exhibit “condor-minium” at the Safari Park. ", "cacondor.jpg"));
            listOfAnimals.Add(new Animal("Crowned Crane", "You don’t need to “crane” your neck to get a good look at these elegant birds.",
                "crane.jpg"));
            listOfAnimals.Add(new Animal("Tiger", "Tiger Trail’s Sambutan Longhouse offers a resting spot for guests, along with more views of the tigers.",
                "tigers.jpg"));
            listOfAnimals.Add(new Animal("Black Rhinos", "Northern white rhinos are extinct in the wild, and only two adult females are left on Earth.",
                "black_rhino.jpg"));
            AnimalsList.ItemsSource = listOfAnimals;
        }

        public Endangered()
        {
            InitializeComponent();
            listOfAnimals = new ObservableCollection<Animal>();
            Populate();
        }


        //Handle the refreshing of each page
        void Handle_Refreshing(Object sender, System.EventArgs e)
        {
            listOfAnimals.Clear();
            Populate();
            AnimalsList.IsRefreshing = false;
        }

        //Remove an item from the animal list
        void Remove_Clicked(object sender, EventArgs e)
        {
            var ani = (MenuItem)sender;
            Animal item = (Animal)ani.CommandParameter;
            listOfAnimals.Remove(item);
        }

        //open page connected to item in the list
        async void ViewCell_Tapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var ani = (ListView)sender;
            Animal item = (Animal)ani.SelectedItem;
            if (item.Title == "Matschie’s Tree Kangaroo")
            {
                Navigation.PushAsync(new TreeKangaroo());
            }
            if (item.Title == "California Condor")
            {
                Navigation.PushAsync(new Condor());
            }
            if (item.Title == "Crowned Crane")
            {
                Navigation.PushAsync(new Crane());
            }
            if (item.Title == "Tiger")
            {
                Navigation.PushAsync(new Tiger());
            }
            if (item.Title == "Black Rhinos")
            {
                Navigation.PushAsync(new Rhino());
            }
        }
    }
}