﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;



namespace CS481_HW4
{
    // [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

        }

        //opens new Threatended page to display animals that are threatended
        private async void Threatended_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Threatened());
        }

        //opens new Stable page to display animals that are endangered
        private async void Endangered_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Endangered());
        }
        
        //opens safari park website
        public void Safari_Clicked(object sender, ItemTappedEventArgs args)
        { 
            Device.OpenUri(new Uri("https://www.sdzsafaripark.org/park-animals-plants"));
        }
       
    }
}